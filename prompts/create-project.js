const prompt = [
  {
    type: "input",
    name: 'projectName',
    message: 'Enter your project name',
    validate: (input) => {
      return input !== '' || "Enter correct project name";
    }
  }, {
    type: "input",
    name: 'themeName',
    message: 'Enter your theme name',
    validate: (input) => {
      return input !== '' || "Enter correct theme name";
    }
  }
]

module.exports = prompt;