const inquirer = require('inquirer');
const util = require('util');
const fs = require('fs');
const exec = util.promisify(require('child_process').exec);
const { execSync } = require('child_process');
const chalk = require('chalk');
const ora = require('ora');

const { program } = require('./commander');
const createProjectPrompt = require('../prompts/create-project');
const spinner = ora();

const css = `.sadasa {
  display: block;
}
`;

const main = async () => {
  program.version('2.2.2').description('BGB CLI');

  program
    .command('create')
    .description('Cloning project')
    .action(async () => {
      try {
        const { projectName, themeName } = await inquirer.prompt(
          createProjectPrompt
        );

        // spinner.text = 'Cloning project';
        // spinner.start();
        // await exec(`git clone https://github.com/michael-disko/web-starter-kit.git ${projectName}`);
        // spinner.succeed(chalk.green(`Project was be cloning`));

        // spinner.text = 'NPM install';
        // spinner.start();
        // await exec('npm install', { cwd: `${projectName}` });
        // spinner.succeed(chalk.green(`NPM was install`));

        spinner.text = 'Create client directory';
        spinner.start();
        await exec(`mkdir src`);
        await exec(`mkdir ${themeName}`, { cwd: 'src' });
        await exec(`mkdir shared`, { cwd: `src/${themeName}` });

        fs.writeFileSync(`src/${themeName}/main.scss`, css);
        fs.writeFileSync(`src/${themeName}/shared/common.scss`, css);
        spinner.succeed(chalk.green(`Client directory was created`));

        process.exit(0);
      } catch (error) {
        spinner.fail(chalk.green(error.stderr || error));
        process.exit(1);
      }
    });

  program.parse(process.argv);

  // const install = spawn('npm', ['install', '--progress'], { cwd: 'cc' });
  // install.stdout.on('data', (data) => {
  //   console.log(chalk.blue(data));
  // });
  // install.stderr.on('data', (data) => {
  //   console.log(chalk.blue(data));
  // });
  // install.on('close', (code) => {
  //   console.log(chalk.green(`Project ${projectName} was be created`));
  // });
};

main();
