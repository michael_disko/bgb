const util = require('util');
const exec = util.promisify(require('child_process').exec);
const program = require("commander");
const chalk = require("chalk");

class CloneProjectTask {

  init(projectName) {
    program.command("clone").description("Cloning project").action(() => {
      console.log(234234233);
      try {
        exec(`git clone https://github.com/michael-disko/web-starter-kit.git ${projectName}`);
        console.log(chalk.green(`Project ${projectName} was be created`));
      } catch (error) {
        console.log(chalk.red(error.stderr || error));
      }
    }).parse(process.argv);
  }
}

module.exports = { CloneProjectTask };
