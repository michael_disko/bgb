const util = require('util');
const exec = util.promisify(require('child_process').exec);
const chalk = require("chalk");

module.exports = async (projectName) => {
  try {
    await exec(`cd ${projectName}`);
    await exec('npm install');
    console.log(chalk.green(`Project ${projectName} was be install`));
  } catch (error) {
    console.log(chalk.red(error.stderr));
  }
};
